#include <sstream>
#include <string>
#include <utility>

#include <doctest/doctest.h>

#include "aoc/solvers.hpp"

using aoc::solvers::solution;

TEST_CASE("Example 01") {
  std::istringstream stream(
      "3   4\n"
      "4   3\n"
      "2   5\n"
      "1   3\n"
      "3   9\n"
      "3   3\n");

  solution solution = aoc::solvers::solve_2024_01(stream);

  CHECK_EQ(std::get<0>(solution), "11");
  CHECK_EQ(std::get<1>(solution), "31");
}
