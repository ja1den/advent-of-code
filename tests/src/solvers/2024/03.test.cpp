#include <sstream>
#include <string>
#include <utility>

#include <doctest/doctest.h>

#include "aoc/solvers.hpp"

using aoc::solvers::solution;

TEST_CASE("Example 01") {
  std::istringstream stream(
      "xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))"
      "\n");

  solution solution = aoc::solvers::solve_2024_03(stream);

  CHECK_EQ(std::get<0>(solution), "161");
}

TEST_CASE("Example 02") {
  std::istringstream stream(
      "xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5)"
      ")\n");

  solution solution = aoc::solvers::solve_2024_03(stream);

  CHECK_EQ(std::get<1>(solution), "48");
}
