#include <sstream>
#include <string>
#include <utility>

#include <doctest/doctest.h>

#include "aoc/solvers.hpp"

using aoc::solvers::solution;

TEST_CASE("Example 01") {
  std::istringstream stream(
      "7 6 4 2 1\n"
      "1 2 7 8 9\n"
      "9 7 6 2 1\n"
      "1 3 2 4 5\n"
      "8 6 4 4 1\n"
      "1 3 6 7 9\n");

  solution solution = aoc::solvers::solve_2024_02(stream);

  CHECK_EQ(std::get<0>(solution), "2");
  CHECK_EQ(std::get<1>(solution), "4");
}
