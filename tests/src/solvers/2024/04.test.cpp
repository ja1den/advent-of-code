#include <sstream>
#include <string>
#include <utility>

#include <doctest/doctest.h>

#include "aoc/solvers.hpp"

using aoc::solvers::solution;

TEST_CASE("Example 01") {
  std::istringstream stream(
      "MMMSXXMASM\n"
      "MSAMXMSMSA\n"
      "AMXSXMAAMM\n"
      "MSAMASMSMX\n"
      "XMASAMXAMM\n"
      "XXAMMXXAMA\n"
      "SMSMSASXSS\n"
      "SAXAMASAAA\n"
      "MAMMMXMMMM\n"
      "MXMXAXMASX\n");

  solution solution = aoc::solvers::solve_2024_04(stream);

  CHECK_EQ(std::get<0>(solution), "18");
  CHECK_EQ(std::get<1>(solution), "9");
}
