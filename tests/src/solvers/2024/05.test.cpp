#include <sstream>
#include <string>
#include <utility>

#include <doctest/doctest.h>

#include "aoc/solvers.hpp"

using aoc::solvers::solution;

TEST_CASE("Example 01") {
  std::istringstream stream(
      "47|53\n"
      "97|13\n"
      "97|61\n"
      "97|47\n"
      "75|29\n"
      "61|13\n"
      "75|53\n"
      "29|13\n"
      "97|29\n"
      "53|29\n"
      "61|53\n"
      "97|53\n"
      "61|29\n"
      "47|13\n"
      "75|47\n"
      "97|75\n"
      "47|61\n"
      "75|61\n"
      "47|29\n"
      "75|13\n"
      "53|13\n"
      "\n"
      "75,47,61,53,29\n"
      "97,61,53,29,13\n"
      "75,29,13\n"
      "75,97,47,61,53\n"
      "61,13,29\n"
      "97,13,75,29,47\n");

  solution solution = aoc::solvers::solve_2024_05(stream);

  CHECK_EQ(std::get<0>(solution), "143");
  CHECK_EQ(std::get<1>(solution), "123");
}
