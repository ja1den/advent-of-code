<div align="center">
  <p>
    <img src="media/icon.svg" alt="Advent of Code" width="128">
  </p>

  <h1>Advent of Code</h1>

  <p>
    My solvers for the Advent of Code puzzles.
  </p>
</div>

## Introduction

This repository contains my solvers for the Advent of Code puzzles.

Each is written in [C++23](https://en.cppreference.com/w/cpp/23).

This repository's structure is based on [TheLartians/ModernCppStarter](https://github.com/TheLartians/ModernCppStarter).

## Usage

```shell
❯ aoc --help
Usage: aoc [options] <event> <day> <input>

Solve a puzzle and print the solution

Arguments:
  event       The event of the puzzle
  day         The day of the puzzle
  input       The input to the puzzle

Options:
  -h, --help  Display help
```

## Contribution

The following packages are required for development:

- `clang++`
- `clang-format` (optional)
- `cmake`
- `cmake-format` (optional)
- `llvm-cov` (optional)

The provided [devenv](https://devenv.sh/) contains the supported versions of these packages.

To activate the environment, install [Nix](https://nixos.org/) and run:

```shell
nix develop --impure
```

To activate the environment automatically upon entering the directory, install [direnv](https://direnv.net/) and grant permission to load the [`.envrc`](.envrc):

```shell
direnv allow
```

### Building

This repository uses [CMake](https://cmake.org/) for build system generation.

#### Generation

To generate a build system:

```shell
cmake -S all/ -B build/
```

To enable code coverage, pass `-DCODE_COVERAGE=ON`.

To generate a `compile_commands.json` for better intellisense, pass `-DCMAKE_EXPORT_COMPILE_COMMANDS=1`.

For example:

```shell
cmake -S all/ -B build/ -DCODE_COVERAGE=ON -DCMAKE_EXPORT_COMPILE_COMMANDS=1
```

#### Usage

To list all available targets:

```shell
cmake --build build/ --target help
```

To build a target:

```shell
cmake --build build/ --target <target>
```
