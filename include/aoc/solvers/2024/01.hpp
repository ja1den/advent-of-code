// IWYU pragma: private, include "aoc/solvers.hpp"

#pragma once

#include <iosfwd>

#include "aoc/solvers/common.hpp"

namespace aoc {

namespace solvers {

solution solve_2024_01(std::istream& stream);

}  // namespace solvers

}  // namespace aoc
