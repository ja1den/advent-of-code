// IWYU pragma: private, include "aoc/solvers.hpp"
// IWYU pragma: friend "aoc/solvers/.*/.*"

#pragma once

#include <iosfwd>
#include <string>
#include <utility>

namespace aoc {

namespace solvers {

using solution = std::pair<std::string, std::string>;
using solver = solution(std::istream&);

}  // namespace solvers

}  // namespace aoc
