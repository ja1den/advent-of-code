#!/usr/bin/env sh

include-what-you-use "$@"

status="$?"

if [ "${status}" -eq 1 ]
then
  exit 0
fi

exit "${status}"
