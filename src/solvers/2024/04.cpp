#include "aoc/solvers/2024/04.hpp"

#include <array>
#include <complex>
#include <cstddef>
#include <istream>
#include <stdexcept>
#include <string>
#include <vector>

#include "aoc/solvers.hpp"

namespace aoc {

namespace solvers {

static const std::array<std::complex<int>, 8> DIRECTIONS = {{
    {-1, 0},   // N
    {-1, 1},   // NE
    {0, 1},    // E
    {1, 1},    // SE
    {1, 0},    // S
    {1, -1},   // SW
    {0, -1},   // W
    {-1, -1},  // NW
}};

solution solve_2024_04(std::istream& stream) {
  std::vector<std::vector<char>> grid;

  for (std::string line; std::getline(stream, line);) {
    grid.emplace_back();

    for (const char& c : line) grid.back().push_back(c);
  }

  int count_xmas = 0;
  int count_masx = 0;

  for (std::size_t r = 0; r < grid.size(); ++r)
    for (std::size_t c = 0; c < grid[r].size(); ++c) {
      switch (grid[r][c]) {
        case 'X':
          for (const std::complex<int>& direction : DIRECTIONS) {
            std::complex<std::size_t> cell = {r, c};

            for (const char& x : std::string("MAS")) {
              cell += direction;

              try {
                if (grid.at(cell.real()).at(cell.imag()) != x)
                  goto next_direction;
              } catch (const std::out_of_range& ex) {
                goto next_direction;
              }
            }

            ++count_xmas;

          next_direction:
          }

          break;

        case 'A':
          try {
            if (((grid.at(r - 1).at(c + 1) == 'M' &&    // NE
                  grid.at(r + 1).at(c - 1) == 'S') ||   // SW
                 (grid.at(r + 1).at(c - 1) == 'M' &&    // SW
                  grid.at(r - 1).at(c + 1) == 'S')) &&  // NE
                ((grid.at(r + 1).at(c + 1) == 'M' &&    // SE
                  grid.at(r - 1).at(c - 1) == 'S') ||   // NW
                 (grid.at(r - 1).at(c - 1) == 'M' &&    // NW
                  grid.at(r + 1).at(c + 1) == 'S')))    // NE
              ++count_masx;
          } catch (const std::out_of_range& ex) {
            continue;
          }

          break;
      }
    }

  return {std::to_string(count_xmas), std::to_string(count_masx)};
}

}  // namespace solvers

}  // namespace aoc
