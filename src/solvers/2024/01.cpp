#include "aoc/solvers/2024/01.hpp"

#include <algorithm>
#include <cstdlib>
#include <istream>
#include <ranges>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

#include "aoc/solvers.hpp"

namespace aoc {

namespace solvers {

solution solve_2024_01(std::istream& stream) {
  std::vector<int> a;
  std::vector<int> b;

  std::unordered_map<int, int> counts;

  for (int x, y; stream >> x >> y;) {
    a.push_back(x);
    b.push_back(y);

    ++counts[y];
  }

  std::ranges::sort(a);
  std::ranges::sort(b);

  int total_distance = 0;
  int similarity_score = 0;

  for (const auto& [x, y] : std::ranges::zip_view(a, b)) {
    total_distance += std::abs(y - x);
    similarity_score += x * counts[x];
  }

  return {std::to_string(total_distance), std::to_string(similarity_score)};
}

}  // namespace solvers

}  // namespace aoc
