#include "aoc/solvers/2024/05.hpp"

#include <algorithm>
#include <sstream>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "aoc/solvers.hpp"

namespace aoc {

namespace solvers {

enum class mode { rules, updates };

solution solve_2024_05(std::istream& stream) {
  mode mode = mode::rules;

  std::unordered_map<int, std::unordered_set<int>> rules;

  const auto comp = [&rules](int a, int b) { return rules.at(a).contains(b); };

  int sum_ready = 0;
  int sum_fixed = 0;

  for (std::string line; std::getline(stream, line);) {
    std::istringstream iss(line);

    switch (mode) {
      case mode::rules: {
        if (line.length() == 0) {
          mode = mode::updates;

          break;
        }

        int x;
        int y;

        iss >> x;
        iss.ignore(1) >> y;

        rules.try_emplace(x);
        rules.try_emplace(y);

        rules[x].insert(y);

        break;
      }

      case mode::updates: {
        std::vector<int> update;

        for (int x; iss >> x; iss.ignore(1)) update.push_back(x);

        if (std::ranges::is_sorted(update, comp))
          sum_ready += update[update.size() / 2];
        else {
          std::ranges::sort(update, comp);

          sum_fixed += update[update.size() / 2];
        }

        break;
      }
    }
  }

  return {std::to_string(sum_ready), std::to_string(sum_fixed)};
}

}  // namespace solvers

}  // namespace aoc
