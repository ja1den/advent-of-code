#include "aoc/solvers/2024/03.hpp"

#include <istream>
#include <regex>
#include <string>
#include <utility>

#include "aoc/solvers.hpp"

namespace aoc {

namespace solvers {

solution solve_2024_03(std::istream& stream) {
  const std::regex instruction_regex(
      "(mul)\\((\\d+),(\\d+)\\)|(do)\\(\\)|(don't)\\(\\)");

  int sum = 0;
  int sum_conditional = 0;

  bool enabled = true;

  for (std::string line; std::getline(stream, line);)
    for (std::sregex_iterator it(line.begin(), line.end(), instruction_regex);
         it != std::sregex_iterator(); ++it) {
      std::smatch match = *it;

      if (match[1] == "mul") {
        int product = std::stoi(match[2]) * std::stoi(match[3]);

        sum += product;
        if (enabled) sum_conditional += product;
      } else
        enabled = match[4] == "do";
    }

  return {std::to_string(sum), std::to_string(sum_conditional)};
}

}  // namespace solvers

}  // namespace aoc
