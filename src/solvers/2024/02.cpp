#include "aoc/solvers/2024/02.hpp"

#include <cstdlib>
#include <sstream>
#include <string>

#include "aoc/solvers.hpp"

namespace aoc {

namespace solvers {

int is_safe(std::string report, int skip = -1) {
  std::istringstream iss(report);

  int i = -1;

  int y;
  int x;

  if (i + 1 == skip) {
    ++i;
    iss >> y;
  }
  ++i;
  iss >> y;
  if (i + 1 == skip) {
    ++i;
    iss >> x;
  }
  ++i;
  iss >> x;

  bool direction = y < x;

  while (iss) {
    if (y < x != direction) return i;

    int d = std::abs(x - y);

    if (d < 1 || 3 < d) return i;

    y = x;
    if (i + 1 == skip) {
      ++i;
      iss >> x;
    }
    ++i;
    iss >> x;
  }

  return -1;
}

solution solve_2024_02(std::istream& stream) {
  int safe = 0;
  int safe_dampened = 0;

  for (std::string report; std::getline(stream, report);) {
    int index = is_safe(report);

    if (index == -1) {
      ++safe;
      ++safe_dampened;
    } else if (is_safe(report, index) == -1 ||
               is_safe(report, index - 1) == -1 ||
               is_safe(report, index - 2) == -1)
      ++safe_dampened;
  }

  return {std::to_string(safe), std::to_string(safe_dampened)};
}

}  // namespace solvers

}  // namespace aoc
