#include <cerrno>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <functional>
#include <iostream>
#include <string>
#include <unordered_map>
#include <utility>

#include "aoc/solvers.hpp"

using aoc::solvers::solution;
using aoc::solvers::solver;

static const std::unordered_map<
    std::string, std::unordered_map<std::string, std::function<solver>>>
    SOLVERS = {{"2024",
                {{"01", aoc::solvers::solve_2024_01},
                 {"02", aoc::solvers::solve_2024_02},
                 {"03", aoc::solvers::solve_2024_03},
                 {"04", aoc::solvers::solve_2024_04},
                 {"05", aoc::solvers::solve_2024_05}}}};

int main(int argc, char* argv[]) {
  for (int i = 0; i < argc; ++i) {
    if (std::strcmp(argv[i], "-h") == 0 ||
        std::strcmp(argv[i], "--help") == 0) {
      std::cout << "Usage: aoc [options] <event> <day> <input>\n";
      std::cout << "\n";
      std::cout << "Solve a puzzle and print the solution\n";
      std::cout << "\n";
      std::cout << "Arguments:\n";
      std::cout << "  event       The event of the puzzle\n";
      std::cout << "  day         The day of the puzzle\n";
      std::cout << "  input       The input to the puzzle\n";
      std::cout << "\n";
      std::cout << "Options:\n";
      std::cout << "  -h, --help  Display help\n";

      return EXIT_SUCCESS;
    }
  }

  std::string event;
  std::string day;
  std::string input;

  switch (argc) {
    case 1:
      std::cerr << "Error: Missing required argument 'event'\n";

      return EXIT_FAILURE;

    case 2:
      std::cerr << "Error: Missing required argument 'day'\n";

      return EXIT_FAILURE;

    case 3:
      std::cerr << "Error: Missing required argument 'input'\n";

      return EXIT_FAILURE;

    case 4:
      if (SOLVERS.contains(argv[1]))
        event = argv[1];
      else {
        std::cerr << "Error: Invalid value \"" << argv[1]
                  << "\" for argument 'event'\n";

        return EXIT_FAILURE;
      }

      if (SOLVERS.at(event).contains(argv[2]))
        day = argv[2];
      else {
        std::cerr << "Error: Invalid value \"" << argv[2]
                  << "\" for argument 'day'\n";

        return EXIT_FAILURE;
      }

      input = argv[3];

      break;

    default:
      std::cerr << "Error: Too many arguments. Expected 3 arguments but got "
                << (argc - 1) << "\n";

      return EXIT_FAILURE;
  }

  errno = 0;

  std::ifstream stream(input);

  if (stream.fail()) {
    std::cerr << "Error: \"" << input << "\": " << std::strerror(errno) << "\n";

    return EXIT_FAILURE;
  }

  solution solution = SOLVERS.at(event).at(day)(stream);

  std::cout << "Part 1: " << std::get<0>(solution) << "\n";
  std::cout << "Part 2: " << std::get<1>(solution) << "\n";

  return EXIT_SUCCESS;
}
