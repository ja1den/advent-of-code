{
  perSystem = {pkgs, ...}: {
    devShells = {
      "default" =
        pkgs.mkShell.override {
          stdenv = pkgs.clangStdenv;
        } {
          packages = [
            # C++
            pkgs.clang-tools
            pkgs.cmake
            pkgs.cmake-format
            (
              (
                let
                  version = "0.23";
                in
                  pkgs.include-what-you-use.overrideAttrs (
                    prev: {
                      inherit version;

                      src = pkgs.fetchurl {
                        url = "${prev.meta.homepage}/downloads/${prev.pname}-${version}.src.tar.gz";
                        hash = "sha256-AATVqRaXF6zy9IEkilv8FcfVXdwrnNx/RhsG6T1Jxz8=";
                      };
                    }
                  )
              )
              .override {
                llvmPackages = pkgs.llvmPackages_19;
              }
            )
            pkgs.libllvm

            # JavaScript
            pkgs.nodejs-slim
            (
              pkgs.pnpm.override {
                withNode = false;
              }
            )

            # Nix
            pkgs.deadnix
            pkgs.nil
            pkgs.statix

            # Shell
            pkgs.shellcheck
          ];
        };
    };
  };
}
